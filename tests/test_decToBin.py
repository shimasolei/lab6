from binaryConverter import decToBinList

def test_decToBinList():
    assert decToBinList(25) == [1, 1, 0, 0, 1]

def test_decToBinList_2():
    assert decToBinList(8) == [1, 0, 0, 0]

def test_decToBinList_3():
    assert decToBinList(0) == [0]